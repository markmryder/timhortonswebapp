package sheridan;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

    

    @Test
    public void testDrinksRegular() {
        List<String> types = new ArrayList<String>( );
        MealsService serv = new MealsService();
        types = serv.getAvailableMealTypes(MealType.DRINKS);         
        assertFalse(types.isEmpty());
    }
    
    @Test
    public void testDrinksException() 
    {
        MealsService serv = new MealsService();
        List<String> types = new ArrayList<String>( );
        types = serv.getAvailableMealTypes(null);      
        assertTrue(types.get(0).equalsIgnoreCase("No Brand Available"));
        
    }
    
    @Test
    public void testDrinksBoundaryIn() 
    {
        MealsService serv = new MealsService();
        List<String> types = new ArrayList<String>( );
        types = serv.getAvailableMealTypes(MealType.DRINKS);
        assertTrue(types.size() > 3);
    }
    
    @Test
    public void testDrinksBoundaryOut() 
    {
        MealsService serv = new MealsService();
        List<String> types = new ArrayList<String>( );
        types = serv.getAvailableMealTypes(null);
        assertTrue(types.size() == 1);
    }

}
